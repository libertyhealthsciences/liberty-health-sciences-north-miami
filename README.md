Everything we do is connected to the pursuit of bettering our communities and our fellow humankind. Through innovative seed-to-sale cannabis processes, we seek to help others live life on their terms through the natural power of cannabis. Call (833) 254-4877 for more information!

Address: 10795 Biscayne Blvd, Miami, FL 33161, USA

Phone: 786-598-2223

Website: https://www.libertyhealthsciences.com/dispensary-locations/north-miami